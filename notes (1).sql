-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 27, 2017 at 04:49 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `notes`
--

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `file_id` int(11) NOT NULL,
  `file_name` varchar(500) NOT NULL,
  `file_dt_uploaded` datetime NOT NULL,
  `file_user_id` int(11) NOT NULL,
  `notes_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`file_id`, `file_name`, `file_dt_uploaded`, `file_user_id`, `notes_id`) VALUES
(1, '2017/b86790b39d9e127fcaa2256f134da01465df73b41.pdf', '2017-10-18 05:47:52', 1, 1),
(2, '2017/a3ebd5d9e3be06530240caf86fc1c1dc9f1e46b41.pdf', '2017-10-18 10:00:27', 1, 5),
(3, '2017/da62a0fdda8a0c50e712bb686c4a19eca41ccf551.png', '2017-10-27 11:06:18', 1, 24);

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

CREATE TABLE `notes` (
  `notes_id` int(11) NOT NULL,
  `notes_title` varchar(50) NOT NULL,
  `notes_description` longtext NOT NULL,
  `notes_added_date` datetime NOT NULL,
  `notes_updated_date` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `notes_status` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notes`
--

INSERT INTO `notes` (`notes_id`, `notes_title`, `notes_description`, `notes_added_date`, `notes_updated_date`, `user_id`, `notes_status`) VALUES
(1, 'Paypal Api Credentials', '1. Login to your PayPal account\r\n    Link: https://www.paypal.com/signin/?country.x=US&locale.x=en_US\r\n2. Click profile on the upper right part\r\n3. Select and click \"profile and settings\"\r\n4. On the left part under My profile select and click \"My setting tools\"\r\n5. Under my business setup find API access\r\n6. Click update in the right part of API Access\r\n7. Under API Access find option 3 (NVP/SOAP API integration)\r\n8. Under option 3 find \"Request API Credentials\" then click it\r\n9. Under Credential API Signature\r\n', '2017-10-18 05:47:52', '2017-10-18 05:47:52', 1, 0),
(2, 'asdasdasdasd', '', '2017-10-18 06:00:22', '2017-10-18 06:00:22', 1, 0),
(3, 'Et cupidatat error et molestiae eius unde laborios', '<blockquote style=\"margin: 0 0 0 40px; border: none; padding: 0px;\"><blockquote style=\"margin: 0 0 0 40px; border: none; padding: 0px;\">Quos commodi numquam vero sit doloribus quis dolor ipsa, veniam, dolorem mollitia sed magni nisi voluptatum nostrum accusamus consequat. Magnam reprehenderit, nostrud officia aut sit doloremque sit, est dolor reprehenderit, sunt, sapiente perferendis quaerat adipisicing eaque doloribus.</blockquote></blockquote>', '2017-10-18 07:07:59', '2017-10-18 07:07:59', 1, 0),
(4, 'Game', 'https://gartic.io/032Au', '2017-10-18 09:29:56', '2017-10-18 09:29:56', 1, 0),
(5, 'asdfas', 'dfsdfasdfasdf', '2017-10-18 10:00:27', '2017-10-18 10:00:27', 1, 0),
(6, 'asfasdfasdfasdfasdf', 'asdfasdfasdf', '2017-10-18 10:16:52', '2017-10-18 10:16:52', 1, 0),
(7, 'sdfasd', 'fasdfsadfasdf', '2017-10-18 10:16:56', '2017-10-18 10:16:56', 1, 0),
(8, 'asdfasdf', 'asdfasdfasdf', '2017-10-18 10:16:59', '2017-10-18 10:16:59', 1, 0),
(9, 'sadfsad', 'fsadfasdf', '2017-10-18 10:17:01', '2017-10-18 10:17:01', 1, 0),
(10, 'fasdfa', 'sdfasdfsa', '2017-10-18 10:17:05', '2017-10-18 10:17:05', 1, 0),
(11, 'sadfasdf', 'asdfadsf', '2017-10-18 10:17:08', '2017-10-18 10:17:08', 1, 0),
(12, 'fsadf', 'sdfasdfasdf', '2017-10-18 10:17:12', '2017-10-18 10:17:12', 1, 0),
(13, 'asdaasdf', 'asdfsadf', '2017-10-18 10:17:16', '2017-10-18 10:17:16', 1, 0),
(14, 'asdfasdf', 'asdfsdasdf', '2017-10-18 10:17:20', '2017-10-18 10:17:20', 1, 0),
(15, 'fasdfasdfasdfas', 'dfasdfasdfasdf', '2017-10-18 10:17:24', '2017-10-18 10:17:24', 1, 0),
(16, 'asdfasdf', 'sadfasdfasdf', '2017-10-18 10:17:28', '2017-10-18 10:17:28', 1, 0),
(17, 'asdfasd', 'fasdfasdfasdfsadf', '2017-10-18 10:18:00', '2017-10-18 10:18:00', 1, 0),
(18, 'sadfas', 'dfasdfasdfasdf', '2017-10-18 10:18:04', '2017-10-18 10:18:04', 1, 0),
(19, 'asdfasdf', 'asdfasdfsadf', '2017-10-18 10:18:08', '2017-10-18 10:18:08', 1, 0),
(20, 'asdfsadf', 'asdfasdfasdf', '2017-10-18 10:18:12', '2017-10-18 10:18:12', 1, 0),
(21, 'sdafsa', 'dfsadfsdf', '2017-10-18 10:18:16', '2017-10-18 10:18:16', 1, 0),
(22, 'asdf', 'sadfasdfasdf', '2017-10-18 10:18:20', '2017-10-18 10:18:20', 1, 0),
(23, 'asdfasdf', 'asdfasdfasdf', '2017-10-19 10:08:57', '2017-10-19 10:08:57', 3, 0),
(24, '123123123', '', '2017-10-27 11:06:18', '2017-10-27 11:06:18', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `uname` varchar(50) NOT NULL,
  `pword` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `fname`, `lname`, `uname`, `pword`) VALUES
(1, 'Rum', 'Siz', '123', '123'),
(2, 'goneqegynu@hotmail.com', 'mukuj@hotmail.com', 'Germaine Kirby', 'Pa$$w0rd!'),
(3, 'genneva', 'mondares', 'genneva', 'mondares');

-- --------------------------------------------------------

--
-- Table structure for table `view_permission`
--

CREATE TABLE `view_permission` (
  `permission_id` int(11) NOT NULL,
  `note_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `view_permission`
--

INSERT INTO `view_permission` (`permission_id`, `note_id`, `user_id`) VALUES
(1, 1, 0),
(2, 2, 0),
(3, 2, 1),
(4, 3, 1),
(5, 3, 1),
(6, 4, 0),
(7, 4, 1),
(8, 5, 0),
(9, 6, 0),
(10, 6, 1),
(11, 7, 0),
(12, 7, 1),
(13, 8, 0),
(14, 8, 1),
(15, 9, 0),
(16, 9, 1),
(17, 10, 0),
(18, 10, 1),
(19, 11, 0),
(20, 11, 1),
(21, 12, 0),
(22, 12, 1),
(23, 13, 0),
(24, 13, 1),
(25, 14, 0),
(26, 14, 1),
(27, 15, 0),
(28, 15, 1),
(29, 16, 0),
(30, 16, 1),
(31, 17, 0),
(32, 17, 1),
(33, 18, 0),
(34, 18, 1),
(35, 19, 0),
(36, 19, 1),
(37, 20, 0),
(38, 20, 1),
(39, 21, 0),
(40, 21, 1),
(41, 22, 0),
(42, 22, 1),
(43, 23, 0),
(44, 23, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`file_id`);

--
-- Indexes for table `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`notes_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `view_permission`
--
ALTER TABLE `view_permission`
  ADD PRIMARY KEY (`permission_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `notes`
--
ALTER TABLE `notes`
  MODIFY `notes_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `view_permission`
--
ALTER TABLE `view_permission`
  MODIFY `permission_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
