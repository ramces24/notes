<?php

class Sitequery extends CI_Model
{
  public function insert($table,$data){
      $this->db->insert($table,$data);
      return $this->db->insert_id();
  }
  public function insertbatch($table,$data){
    $this->db->insert_batch($table,$data);
  }
  public function updatedb($table,$set=array(),$where = array()){
    $this->db->update($table,$set,$where);
  }
  public function selectdb($table,$where,$fields,$join=array(),$limit="",$orderby=array(),$where_in = array(),$group_by = array()){
    $this->db->select($fields);
    $this->db->from($table);
    if(!empty($join)){
        foreach ($join as $key => $value) {
          $this->db->join($key,$value);
        }
    }
    if(!empty($where)){
      $this->db->where($where);
    }
    if(!empty($where_in)){
      $this->db->where_in($where_in['col'],$where_in['field']);
    }
    if(!empty($orderby)){
      foreach ($orderby as $key => $value) {
        $this->db->order_by($key,$value);
      }
    }
    if(!empty($group_by)){
      $this->db->group_by($group_by);
    }
    if(!empty($limit)){
      foreach ($limit as $key => $value) {
          $this->db->limit($key, $value);
      }
    }
    $query = $this->db->get();
     return $query->result_array();
    // return $this->db->last_query();
  }
  public function get_datatables($table, $column_order, $select, $where, $join, $limit, $offset, $search, $order)
  {
  	$this->db->from($table);
  	$columns = $this->db->list_fields($table);
  	if($select){
  		$this->db->select($select);
  	}
  	if($where){
  		$this->db->where($where);
  	}
  	if($join){
  		foreach ($join as $key) {
  			$this->db->join($key['table'], $key['on'], isset($key['option']) ? $key['option'] : 'inner');
  		}
  	}
  	if($search){
  		$this->db->group_start();
  		foreach ($column_order as $item)
  		{
  			$this->db->or_like($item, $search['value']);
  		}
  		$this->db->group_end();
  	}
  	if($order)
  		$this->db->order_by($column_order[$order['0']['column']], $order['0']['dir']);

    $temp = clone $this->db;
    $data['count'] = $temp->count_all_results();

  	if($limit != -1)
  		$this->db->limit($limit, $offset);

  	$query = $this->db->get();
  	$data['data'] = $query->result();

  	$this->db->from($table);
  	$data['count_all'] = $this->db->count_all_results();
  	return $data;
  }
  public function users_select_like($key)
  {
    $this->db->select('*')
             ->from('users')
             ->like('fname',$key)
             ->or_like('lname', $key)
             ->limit(10);
    return $query = $this->db->get()->result_array();
  }

}


 ?>
