<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends My_Controller {
  private $post;
  public function __construct(){
    parent::__construct();
  }
	public function do_ajax($type)
	{
    $this->$type($this->input->post());
	}
}
