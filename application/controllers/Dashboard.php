<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends My_Controller {
  private $post;
  public function __construct(){
    parent::__construct();
    $this->post = $this->input->post();
  }
	public function index()
	{
    $this->display('dashboard');
	}
}
