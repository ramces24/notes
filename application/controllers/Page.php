<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends My_Controller {
  private $post = array();
  private $sess = array();
  public function __construct(){
    parent::__construct();
    $this->post = $this->input->post();
    $this->sess = $this->session->userdata();
  }
  public function index(){
    $data['page_title'] = 'Home';
    $this->display('home',$data);
  }
	public function login()
	{
    $data['error_msg'] = false;
    if(isset($this->post['create_account'])){
      $insrt_data = array(
                          'uname' => $this->post['uname'],
                          'pword' => $this->encryptpword($this->post['pword']),
                          'lname' => $this->post['lname'],
                          'fname' => $this->post['fname']
                        );
      $this->Sitequery->insert('users',$insrt_data);
    }
    if(isset($this->post['login'])){
      $res = $this->Sitequery->selectdb('users',array('pword' => $this->post['pword'],'uname' => $this->post['uname']),'*');
      if(count($res) == 0){
        $data['error_msg'] = true;
      }
      else{
        $this->session->set_userdata('userdata',$this->broke_array($res));
        redirect('page');
      }
    }
		$this->load->view('login',$data);
	}
  public function logout(){
    $this->session->unset_userdata('userdata');
    redirect('page/login');
  }
  public function newnote(){
    $data['page_title'] = 'Add Note';
    if(isset($this->post['create-note'])){
      if(empty($_FILES['notefile']['name'])){
        $nt_id = $this->Sitequery->insert('notes',array('notes_title' => $this->post['title'], 'notes_description' => $this->post['content'], 'notes_added_date' => date('Y-m-d h:i:sa'), 'notes_updated_date' => date('Y-m-d h:i:sa'),'user_id' => $this->sess['userdata']['user_id'] ));
        $this->Sitequery->insertbatch("view_permission",array(array("note_id" => $nt_id, "user_id" => 0),array("note_id" => $nt_id, "user_id" => $this->sess['userdata']['user_id'])));
        $data['success_msg'] = 'Note added successfully';
      }
      else{
        $flenme = sha1(uniqid(mt_rand(), true)).$this->sess['userdata']['user_id'];
        $config['file_name'] = $flenme;
        if(!is_dir('uploads/'.date('Y'))){
            mkdir('uploads/'.date('Y'), 0777, true);
        }
        $config['upload_path']          = './uploads/'.date('Y');
        $config['allowed_types']        = 'gif|jpg|png|php|html|css|js|docx|docs|pdf';
        $config['max_size']             = 1000000;
        $this->load->library('upload');
        $this->upload->initialize($config);
        if ( ! $this->upload->do_upload('notefile'))
            {
              $error = array('error' => $this->upload->display_errors());
              $data['upload_error'] = $error;
            }
        else
          {
              $data['page_title'] = 'Add Note';
              $img_data = $this->upload->data();
              $nt_id = $this->Sitequery->insert('notes',array('notes_title' => $this->post['title'], 'notes_description' => $this->post['content'], 'notes_added_date' => date('Y-m-d h:i:sa'), 'notes_updated_date' => date('Y-m-d h:i:sa'),'user_id' => $this->sess['userdata']['user_id'] ));
              $this->Sitequery->insert('files',array('notes_id' => $nt_id,'file_dt_uploaded' => date('Y-m-d h:i:s'), 'file_name' => date('Y')."/".$config['file_name'].$img_data['file_ext'],'file_user_id' =>$this->session->userdata('userdata')['user_id']));
              if($_POST['access'] == "public"){
                $this->Sitequery->insert('view_permission',array('note_id' => $nt_id,'user_id' => 0));
              }
              if($_POST['access'] == "group" && isset($_POST['usr_id'])){
                array_push($_POST['usr_id'],$this->sess['userdata']['user_id']);
                $usr_id = array();
                foreach ($_POST['usr_id'] as $uid) {
                  $usr_id[] = array('user_id' => $uid, 'note_id' => $nt_id);
                }
                $this->Sitequery->insertbatch('view_permission',$usr_id);
              }
              $data['success_msg'] = 'Note added successfully';
          }
          $this->display('new-note',$data);
      }
    }
    $this->display('new-note',$data);
  }
  public function mynote(){
    $data['page_title'] = 'My Notes';
    $data['notes'] = $this->Sitequery->selectdb('notes',array('user_id' => $this->sess['userdata']['user_id'],'notes_status' => 0),'*');
    $this->display('my-note',$data);
  }
  public function search_note(){
    // $data['notes'] = $this->Sitequery->selectdb("view_permission","","*",array("notes" => "view_permission.note_id = notes.notes_id","users" => "users.user_id = notes.user_id"),"","",array("col" => "view_permission.user_id","field" => array($this->sess['userdata']['user_id'],0) ),array("view_permission.note_id"));
    $this->display('search-note');
  }
  public function isdir(){

  }
}
