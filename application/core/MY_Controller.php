<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_Controller extends CI_Controller {
  public function __construct(){
    parent::__construct();
  }
	public function display($view,$data = array())
	{
    if($this->session->userdata('userdata')){
      $data['account_details'] = $this->user_details();
  		$this->load->view('includes/header',$data);
      $this->load->view($view,$data);
      $this->load->view('includes/footer',$data);
    }
    else{
      redirect('pages/login');
    }
	}
  public function encryptpword($pword){
    return $pword;
  }
  public function broke_array($arr){
    $data_arr = array();
    foreach ($arr as $data) {
      $data_arr = $data;
    }
    return $data_arr;
  }
  public function user_details(){
    $res =  $this->Sitequery->selectdb('users',array('user_id' => $this->session->userdata('userdata')['user_id']),'*');
    return  $this->broke_array($res);
  }
  public function set_note_status($post){
    $this->Sitequery->updatedb('notes',array('notes_status' => $post['note_status']),array('notes_id' => $post['note_id'],'user_id' => $this->session->userdata('userdata')['user_id']));
  }
  public function select_users($post){
    $res = $this->Sitequery->users_select_like($post['keywords']);
    echo json_encode($res);
  }
  public function get_notes(){
		$limit = $this->input->post('length');
		$offset = $this->input->post('start');
		$search = $this->input->post('search');
		$order = $this->input->post('order');
		$draw = $this->input->post('draw');
		$select = '*';
		$column_order = array('notes.notes_title','notes.notes_description','notes.notes_added_date','users.fname');
		$join = array(
			['table'=>'notes', 'on' => 'view_permission.note_id = notes.notes_id'],
			['table'=>'users', 'on' => 'users.user_id = notes.user_id','option'=>'left']
		);
		$list = $this->Sitequery->get_datatables('view_permission',$column_order, $select, null, $join, $limit, $offset ,$search, $order);

		$output = array(
				"draw" => $draw,
				"recordsTotal" => $list['count_all'],
				"recordsFiltered" => $list['count'],
				"data" => $list['data']
		);
		echo json_encode($output);
  }
  public function get_selected_notes(){
    $nt_id = $this->input->post('nt_id');
    $res = $this->Sitequery->selectdb("notes",array("notes_id" => $nt_id),"*");
    echo json_encode($res);
  }
}
