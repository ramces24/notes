<div class="x_panel">
  <div class="x_title">
      <h2>New Note</h2>
      <div class="clearfix">
      </div>
  </div>
  <div class="x_content">
    <table id = "notesdata" class = "table table-striped table-bordered">
        <thead>
          <tr>
              <th>Notes Title</th>
              <th>Date Added</th>
              <th>Date Updated</th>
              <th>Action</th>
          </tr>
        </thead>
        <tbody>
            <?php foreach ($notes as $data): ?>
              <tr>
                  <td><?php echo $data['notes_title']; ?></td>
                  <td><?php echo date('F d Y h:i:sa',strtotime($data['notes_added_date'])); ?></td>
                  <td><?php echo date('F d Y h:i:sa',strtotime($data['notes_updated_date'])); ?></td>
                  <td><button type="button" class="btn btn-xs btn-danger mynote" note-id = "<?php echo $data['notes_id']; ?>" name="button"><i class="fa fa-remove"></i></button><button type="button" class="btn btn-primary btn-xs" name="button"><i class="fa fa-cog" aria-hidden="true"></i></button><button class="btn btn-success btn-xs" type="button" name="button"><i class="fa fa-edit"></i></button></td>
              </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
  </div>
</div>
