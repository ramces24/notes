
      </div>
        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>assets/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url(); ?>assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url(); ?>assets/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url(); ?>assets/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo base_url(); ?>assets/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo base_url(); ?>assets/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo base_url(); ?>assets/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url(); ?>assets/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo base_url(); ?>assets/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo base_url(); ?>assets/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo base_url(); ?>assets/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo base_url(); ?>assets/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="<?php echo base_url(); ?>assets/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url(); ?>assets/vendors/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/sweet-alert/sweetalert.min.js"></script>

    <script src="<?php echo base_url(); ?>assets/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/google-code-prettify/src/prettify.js"></script>

  <script src="<?php echo base_url(); ?>assets/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
  <script src="<?php echo base_url(); ?>assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url(); ?>assets/build/js/custom.min.js"></script>
    <script type="text/javascript">
      $(document).ready(function(){
        var search_note =   $('#notesdata').DataTable({
            "processing": true,
            "serverSide": true,
            "columns":[
              {
                "data":"notes_description"
              },
              {
                "data":"notes_added_date","render":function(data){
                  return moment(data).format("LLLL");
                }
              },
              {
                "data":"fname","render":function(data, type, row, meta){
                  return row['fname']+" "+row['lname'];
                }
              },
              {
                "data":"note_id","render":function(data){
                  return '<button nt_id = "'+data+'" class = "btn btn-success btn-xs show-comment"><i class="fa fa-comments-o" aria-hidden="true"></i></button> <button  class = "view-selected-note btn btn-primary btn-xs" nt_id = "'+data+'" ><i class="fa fa-sticky-note-o" aria-hidden="true"></i></button>';
                }
              }
            ],
            "ajax":{
              url:"<?php echo base_url('ajax/do_ajax/get_notes') ?>",
              type:"POST"
            }

          });
        $(document).on('click','.show-comment',function(){
          search_note.ajax.reload();
        })
        $(document).on('click','.view-selected-note',function(){
          var nt_id = $(this).attr('nt_id');
          $('#show_note').modal("show");
          $.ajax({
            url:"<?php echo base_url(); ?>ajax/do_ajax/get_selected_notes",
            type:"POST",
            data:{nt_id:nt_id},
            success:function(data){
              var json = JSON.parse(data);
              console.log(json);
              alert(json[0].['notes_id']);
            }
          })
        })
        // $('#notesdata').DataTable({});
        $("input[name='notefile']").change(function (e) {
          var $this = $('#filename').text($(this).val().split('\\').pop());
        });
        $(document).on('click','.mynote',function(){
          var note_id = $(this).attr('note-id');
          var notestatus = 1;
          $.ajax({
            url:"<?php echo base_url('ajax/do_ajax/set_note_status'); ?>",
            type:"POST",
            data:{note_id:note_id,note_status:notestatus},
            success:function(data){
              swal({
                  title: "Success !",
                  text: "Successfully moved from trash",
                  type: "success",
                  showCancelButton: false,
                  confirmButtonClass: "btn-success",
                  confirmButtonText: "Close",
                  closeOnConfirm: true
                },function(){
                  location.reload();
                });
            }
          })
        })
        $('#useraccess').on('change',function(){
          if($(this).val() == 'group'){
            $('.container-group').append('<div class="row"><div class="col-md-8"><div class="form-group"><label for="">Search User</label><input type="text" class="form-control" id="searchusers" name="" value=""></div></div><div class="col-md-4"><div class="form-group"><button type="button" style="margin-top:24px" class="btn btn-primary add-usr" name="button">Add</button></div></div></div>');
          }
          else{
            $('.container-group').children().remove();
            $('.searchresults').children().remove();
            $('.userslist').children().remove();
          }
        })
        $(document).on('keyup','#searchusers',function(){
          $('.searchresults').children().remove();
            $.ajax({
              url:"<?php echo base_url('ajax/do_ajax/select_users'); ?>",
              type:"POST",
              data:{keywords:$(this).val()},
              success:function(data){
                if(data == ""){
                  $('.searchresults').children().remove();
                }
                else{
                  var str = "<ul id = 'ulcont'>";
                    var json = JSON.parse(data);
                    $.each(json,function(x,y){
                      str += '<li><a href = "javascript:;" class = "profile" profileid = "'+y.user_id+'" fullnme = "'+y.fname+' '+y.lname+'">'+y.fname+' '+y.lname+'</a></li>';
                    })
                 str += "</ul>";
                 $('.searchresults').append(str);

                }
              }
            })
        })
        $(document).on('click','.profile',function(){
          $('#searchusers').val($(this).attr('fullnme'));
          $('#searchusers').attr('usr_id',$(this).attr('profileid'));
        })
        $(document).on('blur','#searchusers',function(){
          setTimeout(function(){
              $('.searchresults').children().hide();
          },150);
        })
        $(document).on('click','.add-usr',function(){
          var val = $('#searchusers').val();
          var uid = $('#searchusers').attr('usr_id');
          if(val != "" && uid != ""){
            $('.userslist').append('<div class = "sparent"><div class="col-md-8"><div class = "form-group"><input type="hidden" value = "'+uid+'" name = "usr_id[]"><input type="text" class="form-control input-xs" disabled name="" value="'+val+'" uid = "'+uid+'"></div></div><div class="col-md-4"><button type="button" class="btn btn-warning btn-xs remove-selected-user" name="button">Remove</button></div></div>');
            $('#searchusers').val("");
          }
        })
        $(document).on('click','.remove-selected-user',function(){
          $(this).parent().parent().remove();
        })
        $('#editor-one').wysiwyg().on('change', function(){
            $('#cont').val($(this).cleanHtml());
        });
      })
    </script>
    <?php if(isset($success_msg)): ?>
      <script type="text/javascript">
      swal({
          title: "Success !",
          text: "<?php echo $success_msg; ?>",
          type: "success",
          showCancelButton: false,
          confirmButtonClass: "btn-success",
          confirmButtonText: "Close",
          closeOnConfirm: true
        });
      </script>
    <?php endif; ?>
  </body>
</html>
