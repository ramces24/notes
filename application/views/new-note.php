<div class="x_panel">
  <div class="x_title">
      <h2>New Note</h2>
      <div class="clearfix">
      </div>
  </div>
  <div class="x_content">
    <?php if(isset($upload_error)): ?>
      <div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <strong>Error</strong> <?php echo $upload_error['error']; ?>
      </div>
    <?php endif; ?>
    <form class="" action="" method="post" enctype="multipart/form-data">
      <div class="form-group">
        <label for="">Title:</label>
        <input type="text" name="title" class="form-control" value="" required>
      </div>
      <div class="form-group">
        <label for="">Content:</label>
        <!-- <textarea name="content" class="form-control" rows="15" cols="80" required></textarea> -->
        <div class="btn-toolbar editor" data-role="editor-toolbar" data-target="#editor-one">
          <div class="btn-group">
            <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font"><i class="fa fa-font"></i><b class="caret"></b></a>
            <ul class="dropdown-menu">
            </ul>
          </div>

          <div class="btn-group">
            <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font Size"><i class="fa fa-text-height"></i>&nbsp;<b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li>
                <a data-edit="fontSize 5">
                  <p style="font-size:17px">Huge</p>
                </a>
              </li>
              <li>
                <a data-edit="fontSize 3">
                  <p style="font-size:14px">Normal</p>
                </a>
              </li>
              <li>
                <a data-edit="fontSize 1">
                  <p style="font-size:11px">Small</p>
                </a>
              </li>
            </ul>
          </div>

          <div class="btn-group">
            <a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>
            <a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a>
            <a class="btn" data-edit="strikethrough" title="Strikethrough"><i class="fa fa-strikethrough"></i></a>
            <a class="btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>
          </div>

          <div class="btn-group">
            <a class="btn" data-edit="insertunorderedlist" title="Bullet list"><i class="fa fa-list-ul"></i></a>
            <a class="btn" data-edit="insertorderedlist" title="Number list"><i class="fa fa-list-ol"></i></a>
            <a class="btn" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="fa fa-dedent"></i></a>
            <a class="btn" data-edit="indent" title="Indent (Tab)"><i class="fa fa-indent"></i></a>
          </div>

          <div class="btn-group">
            <a class="btn" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a>
            <a class="btn" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a>
            <a class="btn" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a>
            <a class="btn" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="fa fa-align-justify"></i></a>
          </div>

          <div class="btn-group">
            <a class="btn dropdown-toggle" data-toggle="dropdown" title="Hyperlink"><i class="fa fa-link"></i></a>
            <div class="dropdown-menu input-append">
              <input class="span2" placeholder="URL" type="text" data-edit="createLink" />
              <button class="btn" type="button">Add</button>
            </div>
            <a class="btn" data-edit="unlink" title="Remove Hyperlink"><i class="fa fa-cut"></i></a>
          </div>

          <!-- <div class="btn-group">
            <a class="btn" title="Insert picture (or just drag & drop)" id="pictureBtn"><i class="fa fa-picture-o"></i></a>
            <input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" />
          </div> -->

          <div class="btn-group">
            <a class="btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>
            <a class="btn" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a>
          </div>
        </div>

        <div id="editor-one" class="editor-wrapper"></div>

        <textarea name="content" id="cont" style="display:none;"></textarea>
      </div>
      <div class="form-group">
        <label for="">Access:</label>
        <select class="form-control" name="access" id = "useraccess">
            <option value="public">Public</option>
            <option value="group">Select specific users</option>
        </select>
      </div>
      <div class="form-group container-group">

      </div>
      <div class="form-group">
        <div class="row">
          <div class="col-md-8">
            <div class="searchresults">

            </div>
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="row">
            <div class="userslist">

            </div>
        </div>
      </div>
      <div class="form-group">
        <input style="display:none" type="file" id = "notefile" name="notefile" value="">
        <label for="notefile" class="btn btn-primary"><i class="glyphicon glyphicon-upload"></i> Upload File</label>
        <span id = "filename"></span>
      </div>
      <div class="ln_solid"></div>
      <div class="form-group">
            <button type="submit" class="btn btn-success" name="create-note"><i class="glyphicon glyphicon-plus"></i> Create</button>
      </div>

    </form>
  </div>
</div>
