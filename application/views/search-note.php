<div class="x_panel">
  <div class="x_title">
      <h2>Public Notes</h2>
      <div class="clearfix">
      </div>
  </div>
  <div class="x_content">
    <table id = "notesdata" class = "table table-striped table-bordered">
        <thead>
          <tr>
              <th>Notes Title</th>
              <th>Date Added</th>
              <th>Create By</th>
              <th>Action</th>
          </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
  </div>
</div>

<!-- modal -->
<div class="modal fade bs-example-modal-lg" id = "show_note" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Notes Details</h4>
      </div>
      <div class="modal-body">
        <div id="editor-one" class="editor-wrapper" contenteditable="false">asdfasdfasdf</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>

    </div>
  </div>
</div>
